#include <iostream> //Wczytanie zależności
#include <cstdlib>
#include <iomanip>
#include <ctime>
#include <climits>

using namespace std; //Używamy przestrzeni nazw std. Nie występują w kodzie żadne kolizje nazw.

const int w = 10;
const int k = 8;

struct range {
	int begin;
	int end;
};

void fillArray(int array[w][k], range range) {
	for(int i = 0; i < w; i++) {
		for(int j = 0; j < k; j++) {
			array[i][j] = (rand()%(range.end-range.begin+1)) + range.begin;
		}
	}
}

void printArrayRows(int array[w][k]) {
	for(int i = 0; i < w; i++) {
		for(int j = 0; j < k; j++) {
			cout << setw(5) << array[i][j];
		}
		cout << endl;
	}
}

int moreEvenThanOddRowsCount(int array[w][k]) {
	int even[w];
	int return_value = 0;

	//Wypełnienie even[w] zerami i sprawdzenie ile liczb parzystych wystepuje w danym wierszu
	for(int i = 0; i < w; i++) {
		even[i] = 0;
		for(int j = 0; j < k; j++) {
			if(array[i][j]%2 == 0) {
				even[i]++;
			}
		}
	}

	for(int i = 0; i < w; i++) {
		if(even[i] > k/2) {
			return_value++;
		}
	}

	return return_value;
}


/*
* Główna funkcja inicjalizująca program
*/

int main() {
	srand(time(NULL)); //wywołanie srand
	int A[w][k], B[w][k];
	range a, b;
	int rowsWithCriteriaA = 0;
	int rowsWithCriteriaB = 0;

	do
	{
		cout << "Podaj zakres A oddzielajac liczby spacja: ";
		cin >> a.begin >> a.end;
		cout << endl;
	} while(a.begin >= a.end);

	do
	{
		cout << "Podaj zakres B oddzielajac liczby spacja: ";
		cin >> b.begin >> b.end;
		cout << endl;
	} while(b.begin >= b.end);

	fillArray(A, a);
	fillArray(B, b);

	cout << endl << endl;
	printArrayRows(A);
	cout << endl << endl;
	printArrayRows(B);
	cout << endl << endl;

	rowsWithCriteriaA = moreEvenThanOddRowsCount(A);
	rowsWithCriteriaB = moreEvenThanOddRowsCount(B);

	cout << rowsWithCriteriaA << rowsWithCriteriaB;
	if(rowsWithCriteriaA > rowsWithCriteriaB) {
		cout << "Pierwsza tablica ma wiecej elementow spelniajacych kryteria";
	} else if (rowsWithCriteriaA < rowsWithCriteriaB) {
		cout << "Druga tablica ma wiecej elementow spelniajacych kryteria";
	} else {
		cout << "Obie tablice maja tyle samo elementow spelniajacych kryteria";
	}

	cout << endl;

	return 0;
}